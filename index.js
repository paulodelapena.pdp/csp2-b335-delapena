const express = require("express");
const mongoose = require("mongoose");
const userRoutes = require("./routes/user-route");
const cartRoutes = require("./routes/cart-route");
const productRoutes = require("./routes/product-route");
const orderRoutes = require("./routes/order-route")

const port = 4000;

const app = express();


app.use(express.json());
app.use(express.urlencoded({extended:true}));


mongoose.connect("mongodb+srv://admin:admin@b335-delapena.zeyq9jx.mongodb.net/capstone2", 
	{
		useNewUrlParser: true, 
		useUnifiedTopology: true	
	});
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/carts", cartRoutes);
app.use("/orders", orderRoutes);

app.listen(process.env.PORT || port, () => {console.log(`API is now online on port ${process.env.PORT || port}`)});







