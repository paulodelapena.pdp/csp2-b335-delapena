const Order = require("../models/order-model");
const Cart = require("../models/cart-model")
const Users = require("../models/user-model");
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const auth = require("../auth")

module.exports.createOrder = async (req, res) => {
	try {
		// Retrieve the user's cart
		const userCart = await Cart.findOne({userId: req.user._id});
	
		if (!userCart) {
		  return res.status(404).json({ error: 'Cart not found' });
		}
	
		
		const newOrder = new Order({
		  productsOrdered: userCart.cartItems.map(item => ({
			productId: item.productId,
			quantity: item.quantity,
			subtotal: item.subtotal,
		  })),
		  totalPrice: userCart.totalPrice,
		  orderedOn: new Date(),
		  status: 'Pending',
		});
	
		// Save the order to the database
		await newOrder.save();
	
		// clear the cart after ordering
		userCart.cartItems = [];
		userCart.totalPrice = 0;
		await userCart.save();
	
		return res.json({ message: 'Order created successfully' });
	  } catch (error) {
		console.error(error);
		return res.status(500).json({ error: 'Internal Server Error', details: error.message });
	  }
	};


module.exports.retrieveUserOrder = async (req, res) => {
	try {
		// Retrieve all orders for the current logged-in user
		const orders = await Order.findOne({ 'productsOrdered.userId': req.user._id });
	
		return res.json({ message: 'Orders retrieved successfully', orders });
	  } catch (error) {
		console.error(error);
		return res.status(500).json({ error: 'Internal Server Error', details: error.message });
	  }
	};


module.exports.retrieveAllOrders = (req, res) => {
	Order.find()
	.then(result => {
		return res.status(200).send({result})
	})
}