const Cart = require("../models/cart-model")
const Product = require("../models/product-model")
const mongoose = require("mongoose");



module.exports.getCart = async (req, res) => {
	try {
	  // Find the user's cart or create a new one
	  let userCart = await Cart.findOne(req.user._id);
  
	  if (!userCart) {
		return res.json({ message: 'Cart is empty', cart: {} });
	  }
  
	  // Respond with the current contents of the shopping cart
	  res.json({ cart: userCart });
	} catch (error) {
	  console.error(error);
	  res.status(500).json({ error: 'Internal Server Error' });
	}
  };


module.exports.addToCart = async (req,res) => {
		const productId = req.body.productId;
	  
		try {
			// Find the product using the id of product
			const product = await Product.findById(productId);
			
			// if no product found
			if (!product) {
			  return res.status(404).json({ error: 'Product not found' });
			}
		
			// Find the user's cart or create a new one
			let userCart = await Cart.findOne({userId: req.user._id}); // You might want to add user-specific cart logic
		
			if (!userCart) {
			  userCart = new Cart();
			}
		
			// Check if the product is already in the cart
			const cartItem = userCart.cartItems.find(item => item.productId === productId);
		
			if (cartItem) {
			  // If the product is already in the cart, update the quantity and subtotal
			  cartItem.quantity += 1;
			  cartItem.subtotal = cartItem.quantity * product.price;
			} else {
			  // If the product is not in the cart, add a new item
			  userCart.cartItems.push({
				productId: productId,
				quantity: 1,
				subtotal: product.price,
			  });
			}
		
			// Update the total price in the cart
			userCart.totalPrice = userCart.cartItems.reduce((total, item) => total + item.subtotal, 0);
		
			// Save the updated cart
			await userCart.save();
		
			// Respond with the updated cart
			res.json({ message: 'Product added to cart', cart: userCart });
		  } catch (error) {
			console.error(error);
			res.status(500).json({ error: 'Internal Server Error' });
		  }	
		};
		
module.exports.updateProductQuantity = async (req, res) => {
	const productId = req.body.productId;
	  
		try {
			// Find the product using the id of product
			const product = await Product.findById(productId);
			
			// if no product found
			if (!product) {
			  return res.status(404).json({ error: 'Product not found' });
			}

			// Find the user's cart or create a new one
			let userCart = await Cart.findOne({userId: req.user._id});
		
			// Check if the product is in the cart
			const cartItem = userCart.cartItems.find(item => item.productId === productId);
		
			// update the quantity of product
			cartItem.quantity = req.body.quantity
			cartItem.subtotal = cartItem.quantity * product.price;
		
		
			// Update the total price in the cart
			userCart.totalPrice = userCart.cartItems.reduce((total, item) => total + item.subtotal, 0);
		
			// Save the updated cart
			await userCart.save();
		
			// Respond with the updated cart
			res.json({ message: 'Product quantity updated', cart: userCart });
		  } catch (error) {
			console.error(error);
			res.status(500).json({ error: 'Internal Server Error' });
		  }	
		};

module.exports.removeItemFromCart = async (req, res) => {
	const productId = req.params.productId;
	
	try {

		// Find the user's cart
		let userCart = await Cart.findOne({userId: req.user._id});
	
		// Check if the product is in the cart
		const cartItemIndex = userCart.cartItems.findIndex(item => item.productId === productId);
	
		if (cartItemIndex !== -1) {
		  // If the product is in the cart, remove it
		  userCart.cartItems.splice(cartItemIndex, 1);
	
		  // Update the total price in the cart
		  userCart.totalPrice = userCart.cartItems.reduce((total, item) => total + item.subtotal, 0);
	
		  // Save the updated cart
		  await userCart.save();
	
		  // Respond with the updated cart
		  return res.json({ message: 'Product removed from cart', cart: userCart });
		}
	
		// If the product is not in the cart
		return res.status(404).json({ error: 'Product not found in the cart' });
	  } catch (error) {
		console.error(error);
		return res.status(500).json({ error: 'Internal Server Error', details: error.message });
	  }
	};

module.exports.clearCart = async (req, res) => {
	const productId = req.params.productId;
	
	try {

		// Find the user's cart
		let userCart = await Cart.findOne({userId: req.user._id});
	
		userCart.cartItems = []
	
		
		userCart.totalPrice = 0
	
		// Save the updated cart
		await userCart.save();
	
		// Respond with the updated cart
		return res.json({ message: 'Cart has been clear', cart: userCart });
		} catch (error) {
		console.error(error);
		return res.status(500).json({ error: 'Internal Server Error', details: error.message });
		}
	};