const User = require("../models/user-model");
const auth = require("../auth")
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

module.exports.registerUser = async (req, res) => {
	// Check if the email already exists in the database
	await User.findOne({ email: req.body.email })
	  .then(existingUser => {
		if (existingUser) {
		  // If email exists, return an error response
		  return res.status(400).send({ message: "Email already registered" });
		}
  
		// If email doesn't exist, create a new user and save to the database
		let newUser = new User({
		  firstName: req.body.firstName,
		  lastName: req.body.lastName,
		  email: req.body.email,
		  password: bcrypt.hashSync(req.body.password, 10),
		  mobileNo: req.body.mobileNo
		});
  
		return newUser.save()
		  .then((user) => res.status(201).send({ message: "Registered Successfully" }));
	  })
	  .catch(error => {
		// Handle any errors that occur during the process
		console.error("Error during user registration:", error);
		res.status(500).send({ message: "Internal Server Error" });
	  });
  };

module.exports.loginUser = async (req, res) => {
	return await User.findOne({email: req.body.email})
	.then(result => {
		if(!result){
			return res.status(404).send({ error: "No Email Found", });
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
			if (isPasswordCorrect == true) {
				return res.status(200).send({ access : auth.createAccessToken(result)})
			} else {
				return res.status(401).send({ message: "Email and/or password do not match" });
			}
		}
	})
}

module.exports.getProfile = async (req, res) => {
	return await User.findById(req.user.id)
	.then(result =>{

		if(!result){

			return res.status(404).send({error: 'User not found'});
		}
		else{
			result.password = "*****";

			return res.status(200).send({result})
		}
	})
}

module.exports.updateUserAsAdmin = async(req, res) => {
  let updateActiveField = {
        isAdmin: true
    }
    return await User.findByIdAndUpdate(req.params.userId, updateActiveField)
    .then(user => {
    if (!user) {
        return res.status(404).send({ error: 'User not found' });
    } else {
	    return res.status(200).send({ 
	        message: 'User updated to admin successfully', 
	        user: user
    				})
  				}
        })
        .catch(err => {
            console.error("Error in archiving a course: ", err)
            return res.status(500).send({ error: 'Failed to archive course' })
        });
    

};

module.exports.updatePassword = async (req, res) => {

	try {
		const { newPassword } = req.body;

		const { id } = req.user;

		const hashedPassword = await bcrypt.hash(newPassword, 10);
		
		await User.findByIdAndUpdate(id, { password: hashedPassword });

		res.status(200).json({ message: 'Password reset successfully' });

	} catch (error) {

		console.error(error);
		res.status(500).json({ message: 'Internal server error' });

	}

};













