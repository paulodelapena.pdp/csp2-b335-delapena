const Product = require("../models/product-model");
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const auth = require("../auth")

module.exports.createProduct = (req, res) => {
	return Product.findOne({name: req.body.name})
	.then(result => {
		if (result) {
			res.status(400).send({message: 'Product already exist.'})
		} else {
			let newProduct = new Product({
				name: req.body.name,
				description: req.body.description,
				price: req.body.price
			})
			return newProduct.save()
			.then(savedProduct => res.status(200).send({savedProduct}))
			.catch(err => res.status(400).send({message: 'Hala may mali, lagot!'}))
		}
	})
}

module.exports.retrieveAll = (req, res) => {
	return Product.find({})
	.then(products => {res.status(200).send({products})})
	.catch(err => res.status(400).send({message: 'Hala may mali, lagot!'}))
}

module.exports.allActive = (req, res) => {
	Product.find({isActive : true})
	.then(products => {res.status(200).send({products})})
	.catch(err => res.status(400).send({err}))
}

// module.exports.allActive = (req, res) => {

//   Product.find({ isActive : true })
//   .then(products => {
//       // if the result is not null
//       if (products.length > 0){
//           // send the result as a response
//           return res.status(200).send({ products });
//       }
//       // if there are no results found
//       else {
//           // send the message as the response
//           return res.status(200).send({ message: 'No active products found.' })
//       }
//   }).catch(err => res.status(500).send({ error: 'Error finding active products.' }));

// };

module.exports.getProductDetails = (req, res) => {
	Product.findById(req.params.productId)
	.then(product => {
		if(product){
			return res.status(200).send({product})
		} else {
			return res.status(400).send({ message: 'Product not found'})
		}
	})
	.catch(err => {
		res.status(400).send({err})
	})
}

module.exports.updateProductInfo = (req, res) => {
    
    if (!req.body.name || !req.body.description || !req.body.price){
      res.status(400).send({ error: 'Please input required details'})
    }
    let updatedProduct = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }
  
    return Product.findByIdAndUpdate(req.params.productId, updatedProduct)
    .then(result => {
        if(!result){
            return res.status(404).send({ error: 'Product not found' });
        }
        else{
            return res.status(200).send(
                { 
                message: 'Product updated successfully', 
                updatedProduct: updatedProduct
                }
            );
        }
    })
    .catch(err => {
        console.error("Error in updating a course: ", err)
        return res.status(500).send({ error: 'Error in updating a course.' });
    });
}

module.exports.archiveProduct = (req, res) => {
	let updateStatus = {
        isActive: false
    }

    return Product.findByIdAndUpdate(req.params.productId, updateStatus)
    .then(archiveProduct => {
        if(archiveProduct){
        	return res.status(200).send(
                { 
                message: 'Product archived successfully', 
                archiveProduct
                }
            );
        }
        else{
            return res.status(400).send({error: 'Product not found'});
        }
    })
    .catch(err => {
        console.error("Error: ", err)
        return res.status(500).send({ error: 'Error in archiving a product.' });
    });
}

module.exports.activateProduct = (req, res) => {
	let updateStatus = {
        isActive: true
    }
    return Product.findByIdAndUpdate(req.params.productId, updateStatus)
    .then(activateProduct => {
        if(!activateProduct){
            return res.status(400).send({error: 'Product not found'});
        }
        else{
            return res.status(200).send(
                { 
                message: 'Product activated successfully', 
                activateProduct
                }
            );
        }
    })
    .catch(err => {
        return res.status(400).send({ error: 'Error in archiving a product.' });
    });
}

module.exports.searchProductsByName = async (req, res) => {
const productName = req.body.name

  console.log('Searching for product with name:', productName);

  try {
    if (!productName) {
      return res.status(400).json({ error: 'Product name is required in the request body' });
    }

    // Search for products in the database by name
    const searchedProduct = await Product.findOne({
      name: { $regex: new RegExp(productName, 'i') }, // Case-insensitive search
    });

    if (searchedProduct) {
      return res.json({ message: 'Product found', searchedProduct });
    } else {
      return res.json({ message: 'No product found' });
    }
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: 'Internal Server Error', details: error.message });
  }
};

exports.searchProductsByPriceRange = async (req, res) => {
        const { minPrice, maxPrice } = req.body;
      
        console.log('Searching for products within price range:', minPrice, maxPrice);
      
        try {
          if (minPrice === undefined || maxPrice === undefined) {
            return res.status(400).json({ error: 'Both minPrice and maxPrice are required in the request body' });
          }
      
          // Search for products in the database within the specified price range
          const matchingProducts = await Product.find({
            price: { $gte: minPrice, $lte: maxPrice },
          });
      
          if (matchingProducts.length > 0) {
            return res.json({ message: 'Product found', matchingProducts });
          } else {
            return res.json({ message: 'No products found' });
          }
        } catch (error) {
          console.error(error);
          return res.status(500).json({ error: 'Internal Server Error', details: error.message });
        }
      };