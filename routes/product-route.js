const express = require("express");
const router = express.Router();
const productController = require("../controllers/product-controller")

const auth = require("../auth");
const {verify, verifyAdmin} = auth;

router.post("/", productController.createProduct);

router.get("/all", verify, verifyAdmin, productController.retrieveAll);

router.get("/", productController.allActive);

router.get("/searchByName", productController.searchProductsByName);

router.get("/searchByPrice", productController.searchProductsByPriceRange);

router.get("/:productId", productController.getProductDetails);

router.patch("/:productId/update", verify, verifyAdmin, productController.updateProductInfo);

router.patch("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);

router.patch("/:productId/activate", verify, verifyAdmin, productController.activateProduct);



module.exports = router;