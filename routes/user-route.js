const express = require("express");
const router = express.Router();
const userController = require("../controllers/user-controller")
const auth = require("../auth");
const {verify, verifyAdmin} = auth;

router.post("/", userController.registerUser);

router.post("/login", userController.loginUser);

router.get("/details", verify, userController.getProfile);

router.patch("/:userId/set-as-admin", verify, verifyAdmin, userController.updateUserAsAdmin);

router.patch("/update-password", verify, userController.updatePassword);


module.exports = router;