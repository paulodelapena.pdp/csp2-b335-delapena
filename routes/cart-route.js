const express = require("express");
const router = express.Router();
const cartController = require ("../controllers/cart-controller");

const auth = require("../auth");
const {verify, verifyAdmin} = auth;

router.get("/get-cart", verify, cartController.getCart);

router.post("/add-to-cart", verify, cartController.addToCart);

router.post("/update-cart-quantities", verify, cartController.updateProductQuantity);

router.post("/clear-cart", verify, cartController.clearCart);

router.post("/:productId/remove-from-cart", verify, cartController.removeItemFromCart);


module.exports = router;