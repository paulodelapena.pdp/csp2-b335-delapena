const express = require("express");
const router = express.Router();
const orderController = require("../controllers/order-controller")

const auth = require("../auth");
const {verify, verifyAdmin} = auth;


router.post("/checkout", verify, orderController.createOrder);

router.get("/my-orders", verify, orderController.retrieveUserOrder);

router.get("/all-orders", verify, verifyAdmin, orderController.retrieveAllOrders)

module.exports = router;