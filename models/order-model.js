const mongoose = require("mongoose")
const orderSchema = new mongoose.Schema({

<<<<<<< HEAD:s50-s55/models/order-model.js
	userId: {
		type: String
	},
=======
>>>>>>> 3c293dd3db867e3a1e766b0171dd9dda4593b694:models/order-model.js
	productsOrdered: [
		{
		productId: {
			type: String,
			required: [true, "Product ID is Required"]
			},
		quantity: {
			type: Number,
			default: 0
			},
		subtotal: {
			type: Number,
			default: 0
			}
		}
	],
	totalPrice: {
		type: Number,
		default: 0
	},
	orderedOn: {
		type: Date,
		default: Date.now
	},
	status: {
		type: String,
		default: "Pending"
	}
})

module.exports = mongoose.model("Order", orderSchema);