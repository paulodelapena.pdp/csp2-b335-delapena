const mongoose = require("mongoose")
const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: ("First name is required")
	},
	lastName: {
		type: String,
		required: ("Last name is required")
	},
	email: {
		type: String,
		required: ("Email is required")
	},
	password: {
		type: String,
		required: ("Password is required")
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: ("Mobile number is required")
	}
})

module.exports = mongoose.model("User", userSchema);